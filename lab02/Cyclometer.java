// CSE 02 Lab 02 Dillon Mcandrews 2/1/2019
// Cyclometer records time elapsed and rotations of the front wheel, providing information for two bike trips
//
public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args) {
    //input data
    int secsTrip1=480; //provides information for first trip time variable
    int secsTrip2=3220; //provides information for second trip time variable
    int countsTrip1=1561; //provides information for first trip rotations variable
    int countsTrip2=9037; //provides information for second trip rotations variable
    
    //our intermediate variables and output data
    double wheelDiameter=27.0; //provides constant for wheel diameter
    double PI=3.14159; // provides constant PI
    double feetPerMile=5280; // provides constant for feet per mile
    double inchesPerFoot=12; // provides constant for inches in a foot
    double secondsPerMinute=60; // provides constant for seconds in a minute
    double distanceTrip1;
    double distanceTrip2;
    double totalDistance; // no idea
    
    //code for printing variables
    System.out.println ("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had "+ (countsTrip1) + " counts.");
    System.out.println ("Trip 2 took " + (secsTrip2/secondsPerMinute) +" minutes and had "+ (countsTrip2) + " counts.");
    
    //
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //above gives distance in inches
    distanceTrip1/=inchesPerFoot*feetPerMile; 
    // above gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI;
    distanceTrip2/=inchesPerFoot*feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    //printing out the data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  
  }   // end of main method
  
} // end of class