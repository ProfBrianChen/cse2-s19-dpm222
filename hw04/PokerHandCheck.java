/// CSE 02 HW 02 Dillon McAndrews 2/19/2019
// This program is meant to display a poker hand of 5 cards based off of randomly generated values 
// After generating these values, the system should then be able to identify and announce any pairs, two pairs, or three of a kinds. If not are present, should announce a high card hand

public class PokerHandCheck{ //Declaring public class
  public static void main(String args[]){ // Beginning of main method
    
   System.out.println("Your random cards were: ");
    
   // Setting up generation of values and identification of suits
    
    int randomnumber1 = (int)(Math.random()*51)+2; // Assigns a random value to a variable taking the place of 1st card pulled
    int randomnumber2 = (int)(Math.random()*51)+2; // Assigns a random value to a variable taking the place of 2nd card pulled
    int randomnumber3 = (int)(Math.random()*51)+2; // Assigns a random value to a variable taking the place of 3rd card pulled
    int randomnumber4 = (int)(Math.random()*51)+2; // Assigns a random value to a variable taking the place of 4th card pulled
    int randomnumber5 = (int)(Math.random()*51)+2; // Assigns a random value to a variable taking the place of 5th card pulled
    
 
    
    // IF STATEMENTS FOR VALUE 1 :: IF STATEMENTS FOR VALUE 1 :: IF STATEMENTS FOR VALUE 1 :: IF STATEMENTS FOR VALUE 1 :: IF STATEMENTS FOR VALUE 1 :: IF STATEMENTS FOR VALUE1 
    
    
    
    //Beginning of if statement that checks to see if value1 is in diamond suit
    if (randomnumber1 < 14){
      switch ( randomnumber1 ){
        case 1:
          System.out.println("the ace of diamonds.");
        break;
        case 2:
          System.out.println("the 2 of diamonds.");
        break;
        case 3:
          System.out.println("the 3 of diamonds.");
        break;
        case 4:
         System.out.println("the 4 of diamonds.");
        break;  
        case 5:
          System.out.println("the 5 of diamonds.");
        break;
        case 6:
          System.out.println("the 6 of diamonds.");
        break;
        case 7:
          System.out.println("the 7 of diamonds.");
        break;
        case 8:
          System.out.println("the 8 of diamonds.");
        break;   
        case 9:
          System.out.println("the 9 of diamonds.");
          break;
        case 10:
          System.out.println("the 10 of diamonds.");
        break;
        case 11:
          System.out.println("the jack of diamonds.");
        break;
        case 12:
          System.out.println("the queen of diamonds.");
        break;   
        case 13:
          System.out.println("the king of diamonds.");
          break;
      } // end of switch statements that print out diamond card value1
    } // end of IF statement that determines whether randomnumber 1 is Diamond suit
    
    //Beginning of else if statement that checks to see if value1 is in club suit
    else if (randomnumber1 < 27){
      switch ( randomnumber1 ){
        case 14:
          System.out.println("the ace of clubs.");
        break;
        case 15:
          System.out.println("the 2 of clubs.");
        break;
        case 16:
          System.out.println("the 3 of clubs.");
        break;
        case 17:
         System.out.println("the 4 of clubs.");
        break;  
        case 18:
          System.out.println("the 5 of clubs.");
        break;
        case 19:
          System.out.println("the 6 of clubs.");
        break;
        case 20:
          System.out.println("the 7 of clubs.");
        break;
        case 21:
          System.out.println("the 8 of clubs.");
        break;   
        case 22:
          System.out.println("the 9 of clubs.");
          break;
        case 23:
          System.out.println("the 10 of clubs.");
        break;
        case 24:
          System.out.println("the jack of clubs.");
        break;
        case 25:
          System.out.println("the queen of clubs.");
        break;   
        case 26:
          System.out.println("the king of clubs.");
          break;
      } // end of switch statements that prints out club card for value 1
    } // end of IF statement that determines whether randomnumber 1 is club suit
   
    // Beginning of else if statement that checks to see if value 1 is hearts suit
    else if (randomnumber1 < 40){
      switch ( randomnumber1 ){
        case 27:
          System.out.println("the ace of hearts.");
        break;
        case 28:
          System.out.println("the 2 of hearts.");
        break;
        case 29:
          System.out.println("the 3 of hearts.");
        break;
        case 30:
         System.out.println("the 4 of hearts.");
        break;  
        case 31:
          System.out.println("the 5 of hearts.");
        break;
        case 32:
          System.out.println("the 6 of hearts.");
        break;
        case 33:
          System.out.println("the 7 of hearts.");
        break;
        case 34:
          System.out.println("the 8 of hearts.");
        break;   
        case 35:
          System.out.println("the 9 of hearts.");
          break;
        case 36:
          System.out.println("the 10 of hearts.");
        break;
        case 37:
          System.out.println("the jack of hearts.");
        break;
        case 38:
          System.out.println("the queen of hearts.");
        break;   
        case 39:
          System.out.println("the king of hearts.");
          break;          
      } // end of switch statements that prints out hearts card for value1
    } // end of IF statement that determines whether randomnumber 1 is hearts suit
  
    // Beginning of else if statement that checks to see if value 1 is spades suit
    else if (randomnumber1 < 53){
      switch ( randomnumber1 ){
        case 40:
          System.out.println("the ace of spades.");
        break;
        case 41:
          System.out.println("the 2 of spades.");
        break;
        case 42:
          System.out.println("the 3 of spades.");
        break;
        case 43:
         System.out.println("the 4 of spades.");
        break;  
        case 44:
          System.out.println("the 5 of spades.");
        break;
        case 45:
          System.out.println("the 6 of spades.");
        break;
        case 46:
          System.out.println("the 7 of spades.");
        break;
        case 47:
          System.out.println("the 8 of spades.");
        break;   
        case 48:
          System.out.println("the 9 of spades.");
          break;
        case 49:
          System.out.println("the 10 of spades.");
        break;
        case 50:
          System.out.println("the jack of spades.");
        break;
        case 51:
          System.out.println("the queen of spades.");
        break;   
        case 52:
          System.out.println("the king of spades.");
          break;          
    
      } // end of switch statements that prints out spades card for value1
    } //end of IF statement that determines whether value1 is spades suit
    
    
    
// IF STATEMENTS FOR VALUE 2 :: IF STATEMENTS FOR VALUE 2 :: IF STATEMENTS FOR VALUE 2 :: IF STATEMENTS FOR VALUE 2 :: IF STATEMENTS FOR VALUE 2 :: IF STATEMENTS FOR VALUE 2

    
    
//Beginning of if statement that checks to see if value2 is in diamond suit
    if (randomnumber2 < 14){
      switch ( randomnumber2 ){
        case 1:
          System.out.println("the ace of diamonds.");
        break;
        case 2:
          System.out.println("the 2 of diamonds.");
        break;
        case 3:
          System.out.println("the 3 of diamonds.");
        break;
        case 4:
         System.out.println("the 4 of diamonds.");
        break;  
        case 5:
          System.out.println("the 5 of diamonds.");
        break;
        case 6:
          System.out.println("the 6 of diamonds.");
        break;
        case 7:
          System.out.println("the 7 of diamonds.");
        break;
        case 8:
          System.out.println("the 8 of diamonds.");
        break;   
        case 9:
          System.out.println("the 9 of diamonds.");
          break;
        case 10:
          System.out.println("the 10 of diamonds.");
        break;
        case 11:
          System.out.println("the jack of diamonds.");
        break;
        case 12:
          System.out.println("the queen of diamonds.");
        break;   
        case 13:
          System.out.println("the king of diamonds.");
          break;
      } // end of switch statements that print out diamond card value2
    } // end of IF statement that determines whether randomnumber 2 is Diamond suit
    
    //Beginning of else if statement that checks to see if value2 is in club suit
    else if (randomnumber2 < 27){
      switch ( randomnumber2 ){
        case 14:
          System.out.println("the ace of clubs.");
        break;
        case 15:
          System.out.println("the 2 of clubs.");
        break;
        case 16:
          System.out.println("the 3 of clubs.");
        break;
        case 17:
         System.out.println("the 4 of clubs.");
        break;  
        case 18:
          System.out.println("the 5 of clubs.");
        break;
        case 19:
          System.out.println("the 6 of clubs.");
        break;
        case 20:
          System.out.println("the 7 of clubs.");
        break;
        case 21:
          System.out.println("the 8 of clubs.");
        break;   
        case 22:
          System.out.println("the 9 of clubs.");
          break;
        case 23:
          System.out.println("the 10 of clubs.");
        break;
        case 24:
          System.out.println("the jack of clubs.");
        break;
        case 25:
          System.out.println("the queen of clubs.");
        break;   
        case 26:
          System.out.println("the king of clubs.");
          break;
      } // end of switch statements that prints out club card for value 2
    } // end of IF statement that determines whether randomnumber 2 is club suit
   
    // Beginning of else if statement that checks to see if value 2 is hearts suit
    else if (randomnumber2 < 40){
      switch ( randomnumber2 ){
        case 27:
          System.out.println("the ace of hearts.");
        break;
        case 28:
          System.out.println("the 2 of hearts.");
        break;
        case 29:
          System.out.println("the 3 of hearts.");
        break;
        case 30:
         System.out.println("the 4 of hearts.");
        break;  
        case 31:
          System.out.println("the 5 of hearts.");
        break;
        case 32:
          System.out.println("the 6 of hearts.");
        break;
        case 33:
          System.out.println("the 7 of hearts.");
        break;
        case 34:
          System.out.println("the 8 of hearts.");
        break;   
        case 35:
          System.out.println("the 9 of hearts.");
          break;
        case 36:
          System.out.println("the 10 of hearts.");
        break;
        case 37:
          System.out.println("the jack of hearts.");
        break;
        case 38:
          System.out.println("the queen of hearts.");
        break;   
        case 39:
          System.out.println("the king of hearts.");
          break;          
      } // end of switch statements that prints out hearts card for value2
    } // end of IF statement that determines whether randomnumber 2 is hearts suit
  
    // Beginning of else if statement that checks to see if value 2 is spades suit
    else if (randomnumber2 < 53){
      switch ( randomnumber2 ){
        case 40:
          System.out.println("the ace of spades.");
        break;
        case 41:
          System.out.println("the 2 of spades.");
        break;
        case 42:
          System.out.println("the 3 of spades.");
        break;
        case 43:
         System.out.println("the 4 of spades.");
        break;  
        case 44:
          System.out.println("the 5 of spades.");
        break;
        case 45:
          System.out.println("the 6 of spades.");
        break;
        case 46:
          System.out.println("the 7 of spades.");
        break;
        case 47:
          System.out.println("the 8 of spades.");
        break;   
        case 48:
          System.out.println("the 9 of spades.");
          break;
        case 49:
          System.out.println("the 10 of spades.");
        break;
        case 50:
          System.out.println("the jack of spades.");
        break;
        case 51:
          System.out.println("the queen of spades.");
        break;   
        case 52:
          System.out.println("the king of spades.");
          break;          
    
      } // end of switch statements that prints out spades card for value2
    } //end of IF statement that determines whether value2 is spades suit

    
    
// IF STATEMENTS FOR VALUE 3 :: IF STATEMENTS FOR VALUE 3 :: IF STATEMENTS FOR VALUE 3 :: IF STATEMENTS FOR VALUE 3 :: IF STATEMENTS FOR VALUE 3 :: IF STATEMENTS FOR VALUE 3
    

    
//Beginning of if statement that checks to see if value3 is in diamond suit
    if (randomnumber3 < 14){
      switch ( randomnumber3 ){
        case 1:
          System.out.println("the ace of diamonds.");
        break;
        case 2:
          System.out.println("the 2 of diamonds.");
        break;
        case 3:
          System.out.println("the 3 of diamonds.");
        break;
        case 4:
         System.out.println("the 4 of diamonds.");
        break;  
        case 5:
          System.out.println("the 5 of diamonds.");
        break;
        case 6:
          System.out.println("the 6 of diamonds.");
        break;
        case 7:
          System.out.println("the 7 of diamonds.");
        break;
        case 8:
          System.out.println("the 8 of diamonds.");
        break;   
        case 9:
          System.out.println("the 9 of diamonds.");
          break;
        case 10:
          System.out.println("the 10 of diamonds.");
        break;
        case 11:
          System.out.println("the jack of diamonds.");
        break;
        case 12:
          System.out.println("the queen of diamonds.");
        break;   
        case 13:
          System.out.println("the king of diamonds.");
          break;
      } // end of switch statements that print out diamond card value3
    } // end of IF statement that determines whether randomnumber 3 is Diamond suit
    
    //Beginning of else if statement that checks to see if value3 is in club suit
    else if (randomnumber3 < 27){
      switch ( randomnumber3 ){
        case 14:
          System.out.println("the ace of clubs.");
        break;
        case 15:
          System.out.println("the 2 of clubs.");
        break;
        case 16:
          System.out.println("the 3 of clubs.");
        break;
        case 17:
         System.out.println("the 4 of clubs.");
        break;  
        case 18:
          System.out.println("the 5 of clubs.");
        break;
        case 19:
          System.out.println("the 6 of clubs.");
        break;
        case 20:
          System.out.println("the 7 of clubs.");
        break;
        case 21:
          System.out.println("the 8 of clubs.");
        break;   
        case 22:
          System.out.println("the 9 of clubs.");
          break;
        case 23:
          System.out.println("the 10 of clubs.");
        break;
        case 24:
          System.out.println("the jack of clubs.");
        break;
        case 25:
          System.out.println("the queen of clubs.");
        break;   
        case 26:
          System.out.println("the king of clubs.");
          break;
      } // end of switch statements that prints out club card for value 3
    } // end of IF statement that determines whether randomnumber 3 is club suit
   
    // Beginning of else if statement that checks to see if value 3 is hearts suit
    else if (randomnumber3 < 40){
      switch ( randomnumber3 ){
        case 27:
          System.out.println("the ace of hearts.");
        break;
        case 28:
          System.out.println("the 2 of hearts.");
        break;
        case 29:
          System.out.println("the 3 of hearts.");
        break;
        case 30:
         System.out.println("the 4 of hearts.");
        break;  
        case 31:
          System.out.println("the 5 of hearts.");
        break;
        case 32:
          System.out.println("the 6 of hearts.");
        break;
        case 33:
          System.out.println("the 7 of hearts.");
        break;
        case 34:
          System.out.println("the 8 of hearts.");
        break;   
        case 35:
          System.out.println("the 9 of hearts.");
          break;
        case 36:
          System.out.println("the 10 of hearts.");
        break;
        case 37:
          System.out.println("the jack of hearts.");
        break;
        case 38:
          System.out.println("the queen of hearts.");
        break;   
        case 39:
          System.out.println("the king of hearts.");
          break;          
      } // end of switch statements that prints out hearts card for value3
    } // end of IF statement that determines whether randomnumber 3 is hearts suit
  
    // Beginning of else if statement that checks to see if value 3 is spades suit
    else if (randomnumber3 < 53){
      switch ( randomnumber3 ){
        case 40:
          System.out.println("the ace of spades.");
        break;
        case 41:
          System.out.println("the 2 of spades.");
        break;
        case 42:
          System.out.println("the 3 of spades.");
        break;
        case 43:
         System.out.println("the 4 of spades.");
        break;  
        case 44:
          System.out.println("the 5 of spades.");
        break;
        case 45:
          System.out.println("the 6 of spades.");
        break;
        case 46:
          System.out.println("the 7 of spades.");
        break;
        case 47:
          System.out.println("the 8 of spades.");
        break;   
        case 48:
          System.out.println("the 9 of spades.");
          break;
        case 49:
          System.out.println("the 10 of spades.");
        break;
        case 50:
          System.out.println("the jack of spades.");
        break;
        case 51:
          System.out.println("the queen of spades.");
        break;   
        case 52:
          System.out.println("the king of spades.");
          break;          
    
      } // end of switch statements that prints out spades card for value3
    } //end of IF statement that determines whether value3 is spades suit
 
    
    
// IF STATEMENTS FOR VALUE 4 :: IF STATEMENTS FOR VALUE 4 :: IF STATEMENTS FOR VALUE 4 :: IF STATEMENTS FOR VALUE 4 :: IF STATEMENTS FOR VALUE 4 :: IF STATEMENTS FOR VALUE 4

    
    
//Beginning of if statement that checks to see if value4 is in diamond suit
    if (randomnumber4 < 14){
      switch ( randomnumber4 ){
        case 1:
          System.out.println("the ace of diamonds.");
        break;
        case 2:
          System.out.println("the 2 of diamonds.");
        break;
        case 3:
          System.out.println("the 3 of diamonds.");
        break;
        case 4:
         System.out.println("the 4 of diamonds.");
        break;  
        case 5:
          System.out.println("the 5 of diamonds.");
        break;
        case 6:
          System.out.println("the 6 of diamonds.");
        break;
        case 7:
          System.out.println("the 7 of diamonds.");
        break;
        case 8:
          System.out.println("the 8 of diamonds.");
        break;   
        case 9:
          System.out.println("the 9 of diamonds.");
          break;
        case 10:
          System.out.println("the 10 of diamonds.");
        break;
        case 11:
          System.out.println("the jack of diamonds.");
        break;
        case 12:
          System.out.println("the queen of diamonds.");
        break;   
        case 13:
          System.out.println("the king of diamonds.");
          break;
      } // end of switch statements that print out diamond card value4
    } // end of IF statement that determines whether randomnumber 4 is Diamond suit
    
    //Beginning of else if statement that checks to see if value4 is in club suit
    else if (randomnumber4 < 27){
      switch ( randomnumber4 ){
        case 14:
          System.out.println("the ace of clubs.");
        break;
        case 15:
          System.out.println("the 2 of clubs.");
        break;
        case 16:
          System.out.println("the 3 of clubs.");
        break;
        case 17:
         System.out.println("the 4 of clubs.");
        break;  
        case 18:
          System.out.println("the 5 of clubs.");
        break;
        case 19:
          System.out.println("the 6 of clubs.");
        break;
        case 20:
          System.out.println("the 7 of clubs.");
        break;
        case 21:
          System.out.println("the 8 of clubs.");
        break;   
        case 22:
          System.out.println("the 9 of clubs.");
          break;
        case 23:
          System.out.println("the 10 of clubs.");
        break;
        case 24:
          System.out.println("the jack of clubs.");
        break;
        case 25:
          System.out.println("the queen of clubs.");
        break;   
        case 26:
          System.out.println("the king of clubs.");
          break;
      } // end of switch statements that prints out club card for value 4
    } // end of IF statement that determines whether randomnumber 4 is club suit
   
    // Beginning of else if statement that checks to see if value 4 is hearts suit
    else if (randomnumber4 < 40){
      switch ( randomnumber4 ){
        case 27:
          System.out.println("the ace of hearts.");
        break;
        case 28:
          System.out.println("the 2 of hearts.");
        break;
        case 29:
          System.out.println("the 3 of hearts.");
        break;
        case 30:
         System.out.println("the 4 of hearts.");
        break;  
        case 31:
          System.out.println("the 5 of hearts.");
        break;
        case 32:
          System.out.println("the 6 of hearts.");
        break;
        case 33:
          System.out.println("the 7 of hearts.");
        break;
        case 34:
          System.out.println("the 8 of hearts.");
        break;   
        case 35:
          System.out.println("the 9 of hearts.");
          break;
        case 36:
          System.out.println("the 10 of hearts.");
        break;
        case 37:
          System.out.println("the jack of hearts.");
        break;
        case 38:
          System.out.println("the queen of hearts.");
        break;   
        case 39:
          System.out.println("the king of hearts.");
          break;          
      } // end of switch statements that prints out hearts card for value4
    } // end of IF statement that determines whether randomnumber 4 is hearts suit
  
    // Beginning of else if statement that checks to see if value 4 is spades suit
    else if (randomnumber4 < 53){
      switch ( randomnumber4 ){
        case 40:
          System.out.println("the ace of spades.");
        break;
        case 41:
          System.out.println("the 2 of spades.");
        break;
        case 42:
          System.out.println("the 3 of spades.");
        break;
        case 43:
         System.out.println("the 4 of spades.");
        break;  
        case 44:
          System.out.println("the 5 of spades.");
        break;
        case 45:
          System.out.println("the 6 of spades.");
        break;
        case 46:
          System.out.println("the 7 of spades.");
        break;
        case 47:
          System.out.println("the 8 of spades.");
        break;   
        case 48:
          System.out.println("the 9 of spades.");
          break;
        case 49:
          System.out.println("the 10 of spades.");
        break;
        case 50:
          System.out.println("the jack of spades.");
        break;
        case 51:
          System.out.println("the queen of spades.");
        break;   
        case 52:
          System.out.println("the king of spades.");
          break;     
    } // end of switch statements that prints out spades card for value 4
    } // end of IF statement that determines whether randomnumber 4 is spades suit
    
    
    
// IF STATEMENTS FOR VALUE 5 :: IF STATEMENTS FOR VALUE 5 :: IF STATEMENTS FOR VALUE 5 :: IF STATEMENTS FOR VALUE 5 :: IF STATEMENTS FOR VALUE 5 :: IF STATEMENTS FOR VALUE 5

    
    
//Beginning of if statement that checks to see if value5 is in diamond suit
    if (randomnumber5 < 14){
      switch ( randomnumber5 ){
        case 1:
          System.out.println("the ace of diamonds.");
        break;
        case 2:
          System.out.println("the 2 of diamonds.");
        break;
        case 3:
          System.out.println("the 3 of diamonds.");
        break;
        case 4:
         System.out.println("the 4 of diamonds.");
        break;  
        case 5:
          System.out.println("the 5 of diamonds.");
        break;
        case 6:
          System.out.println("the 6 of diamonds.");
        break;
        case 7:
          System.out.println("the 7 of diamonds.");
        break;
        case 8:
          System.out.println("the 8 of diamonds.");
        break;   
        case 9:
          System.out.println("the 9 of diamonds.");
          break;
        case 10:
          System.out.println("the 10 of diamonds.");
        break;
        case 11:
          System.out.println("the jack of diamonds.");
        break;
        case 12:
          System.out.println("the queen of diamonds.");
        break;   
        case 13:
          System.out.println("the king of diamonds.");
          break;
      } // end of switch statements that print out diamond card value5
    } // end of IF statement that determines whether randomnumber 5 is Diamond suit
    
    //Beginning of else if statement that checks to see if value 5 is in club suit
    else if (randomnumber5 < 27){
      switch ( randomnumber5 ){
        case 14:
          System.out.println("the ace of clubs.");
        break;
        case 15:
          System.out.println("the 2 of clubs.");
        break;
        case 16:
          System.out.println("the 3 of clubs.");
        break;
        case 17:
         System.out.println("the 4 of clubs.");
        break;  
        case 18:
          System.out.println("the 5 of clubs.");
        break;
        case 19:
          System.out.println("the 6 of clubs.");
        break;
        case 20:
          System.out.println("the 7 of clubs.");
        break;
        case 21:
          System.out.println("the 8 of clubs.");
        break;   
        case 22:
          System.out.println("the 9 of clubs.");
          break;
        case 23:
          System.out.println("the 10 of clubs.");
        break;
        case 24:
          System.out.println("the jack of clubs.");
        break;
        case 25:
          System.out.println("the queen of clubs.");
        break;   
        case 26:
          System.out.println("the king of clubs.");
          break;
      } // end of switch statements that prints out club card for value 5
    } // end of IF statement that determines whether randomnumber 5 is club suit
   
    // Beginning of else if statement that checks to see if value 5 is hearts suit
    else if (randomnumber5 < 40){
      switch ( randomnumber5 ){
        case 27:
          System.out.println("the ace of hearts.");
        break;
        case 28:
          System.out.println("the 2 of hearts.");
        break;
        case 29:
          System.out.println("the 3 of hearts.");
        break;
        case 30:
         System.out.println("the 4 of hearts.");
        break;  
        case 31:
          System.out.println("the 5 of hearts.");
        break;
        case 32:
          System.out.println("the 6 of hearts.");
        break;
        case 33:
          System.out.println("the 7 of hearts.");
        break;
        case 34:
          System.out.println("the 8 of hearts.");
        break;   
        case 35:
          System.out.println("the 9 of hearts.");
          break;
        case 36:
          System.out.println("the 10 of hearts.");
        break;
        case 37:
          System.out.println("the jack of hearts.");
        break;
        case 38:
          System.out.println("the queen of hearts.");
        break;   
        case 39:
          System.out.println("the king of hearts.");
          break;          
      } // end of switch statements that prints out hearts card for value5
    } // end of IF statement that determines whether randomnumber 5 is hearts suit
  
    // Beginning of else if statement that checks to see if value 5 is spades suit
    else if (randomnumber5 < 53){
      switch ( randomnumber5 ){
        case 40:
          System.out.println("the ace of spades.");
        break;
        case 41:
          System.out.println("the 2 of spades.");
        break;
        case 42:
          System.out.println("the 3 of spades.");
        break;
        case 43:
         System.out.println("the 4 of spades.");
        break;  
        case 44:
          System.out.println("the 5 of spades.");
        break;
        case 45:
          System.out.println("the 6 of spades.");
        break;
        case 46:
          System.out.println("the 7 of spades.");
        break;
        case 47:
          System.out.println("the 8 of spades.");
        break;   
        case 48:
          System.out.println("the 9 of spades.");
          break;
        case 49:
          System.out.println("the 10 of spades.");
        break;
        case 50:
          System.out.println("the jack of spades.");
        break;
        case 51:
          System.out.println("the queen of spades.");
        break;   
        case 52:
          System.out.println("the king of spades.");
          break;     
    } // end of switch statements that prints out spades card for value 5
  } // end of IF statement that determines whether randomnumber 5 is spades suit

    
    
    
    
    
// DETERMINING WHETHER THERE ARE PAIRS, PAIRS OF TWO, OR THREE OF A KINDS :: DETERMINING WHETHER THERE ARE PAIRS, PAIRS OF TWO, OR THREE OF A KINDS :: DETERMINING WHETHER THERE ARE PAIRS, PAIRS OF TWO, OR THREE OF A KINDS ::               
    
    
    
    

// IF STATEMENTS FOR FINDING PAIRS

    if (randomnumber1 == randomnumber2){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 13 == randomnumber2){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 26 == randomnumber2){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 39 == randomnumber2){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 39 == randomnumber2){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 26 == randomnumber2){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 13 == randomnumber2){
      System.out.println("You have a pair!");
    }
   
  if (randomnumber1 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 13 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 26 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 39 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 39 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 26 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 13 == randomnumber3){
      System.out.println("You have a pair!");
    }
  
  if (randomnumber1 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 13 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 26 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 39 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 39 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 26 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 13 == randomnumber4){
      System.out.println("You have a pair!");
    }
  
 if (randomnumber1 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 + 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber1 - 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    
 if (randomnumber2 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 13 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 26 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 39 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 39 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 26 == randomnumber3){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 13 == randomnumber3){
      System.out.println("You have a pair!");
    }
 
 if (randomnumber2 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 13 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 26 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 39 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 39 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 26 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 13 == randomnumber4){
      System.out.println("You have a pair!");
    }
    
 if (randomnumber2 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 + 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber2 - 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    
 if (randomnumber3 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 + 13 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 + 26 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 + 39 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 - 39 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 - 26 == randomnumber4){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 - 13 == randomnumber4){
      System.out.println("You have a pair!");
    }
    
if (randomnumber3 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 + 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 + 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 + 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 - 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 - 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber3 - 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    
if (randomnumber4 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber4 + 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber4 + 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber4 + 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber4 - 39 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber4 - 26 == randomnumber5){
      System.out.println("You have a pair!");
    }
    else if (randomnumber4 - 13 == randomnumber5){
      System.out.println("You have a pair!");
    }
    
    
    
// IF STATEMENTS FOR DETECTING TWO PAIRS
    
    
    
    
    
    
// IF STATEMENTS FOR DETECTING THREE OF A KINDS

    if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 + 13 == randomnumber2) && (randomnumber2 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 + 26 == randomnumber2) && (randomnumber2 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 + 39 == randomnumber2) && (randomnumber2 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber1 - 13 == randomnumber2) && (randomnumber2 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 - 26 == randomnumber2) && (randomnumber2 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 - 39 == randomnumber2) && (randomnumber2 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber1 == randomnumber2 + 13) && (randomnumber1 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 + 26) && (randomnumber1 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 + 39) && (randomnumber1 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 - 13) && (randomnumber1 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 - 26) && (randomnumber1 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 == randomnumber2 - 39) && (randomnumber1 == randomnumber3)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber3 + 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber3 + 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber3 + 39)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber3 - 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber3 - 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber3 - 39)){
  System.out.println("You have a three of a kind!");  
}   

    
    
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 + 13 == randomnumber2) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 + 26 == randomnumber2) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 + 39 == randomnumber2) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber1 - 13 == randomnumber2) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 - 26 == randomnumber2) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 - 39 == randomnumber2) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber1 == randomnumber2 + 13) && (randomnumber1 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 + 26) && (randomnumber1 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 + 39) && (randomnumber1 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 - 13) && (randomnumber1 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 - 26) && (randomnumber1 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 == randomnumber2 - 39) && (randomnumber1 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber4 + 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber4 + 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber4 + 39)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber4 - 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber4 - 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber4 - 39)){
  System.out.println("You have a three of a kind!");  
}   

    
    
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 + 13 == randomnumber2) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 + 26 == randomnumber2) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 + 39 == randomnumber2) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber1 - 13 == randomnumber2) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 - 26 == randomnumber2) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 - 39 == randomnumber2) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber1 == randomnumber2 + 13) && (randomnumber1 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 + 26) && (randomnumber1 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 + 39) && (randomnumber1 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 - 13) && (randomnumber1 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2 - 26) && (randomnumber1 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber1 == randomnumber2 - 39) && (randomnumber1 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber5 + 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber5 + 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber5 + 39)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber5 - 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber5 - 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber1 == randomnumber2) && (randomnumber2 == randomnumber5 - 39)){
  System.out.println("You have a three of a kind!");  
}   
    

    
    
 if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 + 13 == randomnumber3) && (randomnumber3 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 + 26 == randomnumber3) && (randomnumber3 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber2 + 39 == randomnumber3) && (randomnumber3 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber2 - 13 == randomnumber3) && (randomnumber3 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 - 26 == randomnumber3) && (randomnumber3 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber2 - 39 == randomnumber3) && (randomnumber3 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber2 == randomnumber3 + 13) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 + 26) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 + 39) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 - 13) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 - 26) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber2 == randomnumber3 - 39) && (randomnumber2 == randomnumber4)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber4 + 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber4 + 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber4 + 39)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber4 - 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber4 - 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber4 - 39)){
  System.out.println("You have a three of a kind!");  
}   

 if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 + 13 == randomnumber3) && (randomnumber3 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 + 26 == randomnumber3) && (randomnumber3 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber2 + 39 == randomnumber3) && (randomnumber3 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber2 - 13 == randomnumber3) && (randomnumber3 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 - 26 == randomnumber3) && (randomnumber3 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber2 - 39 == randomnumber3) && (randomnumber3 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}       
if ((randomnumber2 == randomnumber3 + 13) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 + 26) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 + 39) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 - 13) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3 - 26) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}    
if ((randomnumber2 == randomnumber3 - 39) && (randomnumber2 == randomnumber5)){
  System.out.println("You have a three of a kind!");
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber5 + 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber5 + 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber5 + 39)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber5 - 13)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber5 - 26)){
  System.out.println("You have a three of a kind!");    
}
if ((randomnumber2 == randomnumber3) && (randomnumber3 == randomnumber5 - 39)){
  System.out.println("You have a three of a kind!");  
}   
    
    
    
    
    
    
    
    
    
    
    } // end of main method
   
    } // end of public class
        
      
      
    
    
    


