//////////////
//// CSE 02 WelcomeClass
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Welcome text to terminal window
    System.out.println("-----------");
    System.out.println("| Welcome |");
    System.out.println("-----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-D--P--M--2--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v ");
    
  }
  
}