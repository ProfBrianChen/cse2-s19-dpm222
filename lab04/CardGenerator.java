/// CSE 02 HW 02 Dillon McAndrews 2/18/2019
// This program is meant to randomly generate any number between 1-52, with certain sections representing differenting suits (i.e. 1-13 = diamonds)
// When completed, the program should be able to print to terminal a combination of a random number card with the correct affiliated suit

//Start of program
public class CardGenerator {
  public static void main(String args[]) {
    // Start of main method
    
    int randomNumber = (int) (Math.random() * 40); // applies a random math number generator to the int variable int 
    
    // Setting up if statements for all values between 1 and 13 (DIAMONDS) 
 
        if (randomNumber < 14){ // beginning of if statement entailing all cards in diamond suit
        
        switch ( randomNumber ){ // beginning of switch statement to determine which card in diamond suit had been pulled
          case 1 :
          System.out.println ("You pulled an ace of diamonds.");
            break;
          case 2 :
            System.out.println ("You pulled a 2 of diamonds.");
            break;
          case 3 :
            System.out.println ("You pulled a 3 of diamonds.");
            break;
          case 4 :
            System.out.println ("You pulled a 4 of diamonds.");
            break;
          case 5 :
            System.out.println ("You pulled a 5 of diamonds.");
            break;
          case 6 :
            System.out.println ("You pulled a 6 of diamonds.");
            break;
          case 7 :
            System.out.println ("You pulled a 7 of diamonds.");
            break;
          case 8 :
            System.out.println ("You pulled a 8 of diamonds.");
            break;
          case 9 :
            System.out.println ("You pulled a 9 of diamonds.");
            break;
          case 10 :
            System.out.println ("You pulled a 10 of diamonds.");
            break;
          case 11 :
            System.out.println ("You pulled a jack of diamonds.");
            break;
          case 12 :
            System.out.println ("You pulled a queen of diamonds.");
            break;
          case 13 :
            System.out.println ("You pulled a king of diamonds.");
            break;
       
        } //end of switch statement for 1-13
        } // end of if statement for values smaller than 14
    
    else if (randomNumber < 27){ // beginning of if statement entailing all cards in clubs suit
      
      switch ( randomNumber ){ // beginning of switch statement to determine which card in club suit had been pulled
          case 14 :
          System.out.println ("You pulled an ace of clubs.");
            break;
          case 15 :
            System.out.println ("You pulled a 2 of clubs.");
            break;
          case 16 :
            System.out.println ("You pulled a 3 of clubs.");
            break;
          case 17 :
            System.out.println ("You pulled a 4 of clubs.");
            break;
          case 18 :
            System.out.println ("You pulled a 5 of clubs.");
            break;
          case 19 :
            System.out.println ("You pulled a 6 of clubs.");
            break;
          case 20 :
            System.out.println ("You pulled a 7 of clubs.");
            break;
          case 21 :
            System.out.println ("You pulled a 8 of clubs.");
            break;
          case 22 :
            System.out.println ("You pulled a 9 of clubs.");
            break;
          case 23 :
            System.out.println ("You pulled a 10 of clubs.");
            break;
          case 24 :
            System.out.println ("You pulled a jack of clubs.");
            break;
          case 25 :
            System.out.println ("You pulled a queen of clubs.");
            break;
          case 26 :
            System.out.println ("You pulled a king of clubs.");
            break;
      } // end of switch statement for clubs suit
    } // end of if statement for values between 14 and 26
   
    else if (randomNumber < 40){
      switch ( randomNumber ){
           case 27 :
          System.out.println ("You pulled an ace of hearts.");
            break;
          case 28 :
            System.out.println ("You pulled a 2 of hearts.");
            break;
          case 29 :
            System.out.println ("You pulled a 3 of hearts.");
            break;
          case 30 :
            System.out.println ("You pulled a 4 of hearts.");
            break;
          case 31 :
            System.out.println ("You pulled a 5 of hearts.");
            break;
          case 32 :
            System.out.println ("You pulled a 6 of hearts.");
            break;
          case 33 :
            System.out.println ("You pulled a 7 of hearts.");
            break;
          case 34 :
            System.out.println ("You pulled a 8 of hearts.");
            break;
          case 35 :
            System.out.println ("You pulled a 9 of hearts.");
            break;
          case 36 :
            System.out.println ("You pulled a 10 of hearts.");
            break;
          case 37 :
            System.out.println ("You pulled a jack of hearts.");
            break;
          case 38 :
            System.out.println ("You pulled a queen of hearts.");
            break;
          case 39 :
            System.out.println ("You pulled a king of hearts.");
            break;
         
      } // end of switch statement for hearts suit
    } // end of if statement for values between 27 and 39
    
    else if (randomNumber < 53){
      switch ( randomNumber ){
          case 40 :
          System.out.println ("You pulled an ace of spades.");
            break;
          case 41 :
            System.out.println ("You pulled a 2 of spades.");
            break;
          case 42 :
            System.out.println ("You pulled a 3 of spades.");
            break;
          case 43 :
            System.out.println ("You pulled a 4 of spades.");
            break;
          case 44 :
            System.out.println ("You pulled a 5 of spades.");
            break;
          case 45 :
            System.out.println ("You pulled a 6 of spades.");
            break;
          case 46 :
            System.out.println ("You pulled a 7 of spades.");
            break;
          case 47 :
            System.out.println ("You pulled a 8 of spades.");
            break;
          case 48 :
            System.out.println ("You pulled a 9 of spades.");
            break;
          case 49 :
            System.out.println ("You pulled a 10 of spades.");
            break;
          case 50 :
            System.out.println ("You pulled a jack of spades.");
            break;
          case 51 :
            System.out.println ("You pulled a queen of spades.");
            break;
          case 52 :
            System.out.println ("You pulled a king of spades.");
            break;
          
      } // end of switch statemet for spades suit
    } // end of switch statement for values between 40 and 52
    
    
  } // end of main method
} // end of public class