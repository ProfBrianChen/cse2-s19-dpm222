///// CSE 02 HW 03 Dillon McAndrews 2/12/2019
// This program allows a user to input any measurement in meters which will then be converted to inches and printed back out to the terminal

import java.util.Scanner; // Imports Scanner program to be used to detect user input
  
public class Convert {
public static void main(String[] args){
// Start of main method

// Scanner Set Up
Scanner myScanner = new Scanner ( System.in ); // Declares Scanner for the terminal
System.out.print ("Please input a measurement in meters: "); // Displays prompt for user to provide measured value in meters
double measureInMeters = myScanner.nextDouble () ; // Sets up scanner to detect input and place into a double variable

// Setting up variables for calculation
double inchesInaMeter = 39.37; // Provides value to be used for the amount of inches in 1 meter 
double totalMeters = measureInMeters; // Gives a double value to place the provided meter measurement to be used for calculation
double totalInches = measureInMeters * inchesInaMeter; // Provides variable to hold calculation of measurement in inches
  
// Printing results to the terminal
System.out.println ( totalMeters + " meters is " + (int)(totalInches * 100) / 100.00 + " inches."); // Prints information to the terminal for the user to see conversion

} // End of main method

} // End of public class