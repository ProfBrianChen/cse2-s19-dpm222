// ///// CSE 02 HW 03 Dillon McAndrews 2/12/2019
// This program allows a user to input any measurements of length, width, and height of box to calculate and receive the volume of the box

import java.util.Scanner; // Imports Scanner program to be used to detect input given by the user

public class BoxVolume {
  public static void main(String[] args) {
// Start of main method
// Scanner Setup
   Scanner myScanner = new Scanner ( System.in );
  
    // Setting up prompts for user
    System.out.print ("The width side of the box is: "); // Prints prompt for user to input value for width of side of the box to the terminal
    int widthofBox = myScanner.nextInt () ; // Tells Scanner to document input and place into a double variable
    
    System.out.print ("The length of the box is: "); // Prints prompt for user to input value for length of the box to the terminal
    int lengthofBox = myScanner.nextInt () ; // Tells Scanner to document input and place into a double variable
    
    System.out.print ("The height of the box is: "); // Prints prompt for user to input value for box height to the terminal 
    int heightofBox = myScanner.nextInt () ; // Tells Scanner to document input and place into a double variable
    
    //Setting up variables for calculation
    int volumeofBox = widthofBox * heightofBox * lengthofBox; // Calculates volume of the box and places into a double variable
    
    //Printing results to terminal
    System.out.println ("The volume inside the box is: " + volumeofBox);
    
  } // End of main method
  
} // End of public class