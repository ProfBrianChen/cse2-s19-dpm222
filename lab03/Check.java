/// CSE 02 HW 02 Dillon McAndrews 2/8/2019
// This program is intended to calculate the original amount of a check, percentage tip, and number of ways the check is meant to be split. 
// Finally, how much each person in the group needs to spend to cover the whole amount of the check

import java.util.Scanner; // Imports Scanner program

//Creating public class and main method
public class Check {
  // main method required for every program
  public static void main(String[] args){
  
  Scanner myScanner = new Scanner ( System.in ); // Declares Scanner for the program to run
  System.out.print("Enter the original cost of the check (in form xx.xx): "); // Prompts user for original cost of check
  double checkCost = myScanner.nextDouble() ; // Sets up scanner to detect variable input by user and place into a double value
  
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // Prompts user for percentage tip wished to be paid
  double tipPercent = myScanner.nextDouble() ; // Sets up scanner to detect variable input by user and place into a double value
  tipPercent /= 100; // Converts percentage into decimal value
  
  System.out.print("Enter the number of people who went out to dinner: "); // Prompts user for number of people who went to dinner (that will split the check)
  int numPeople = myScanner.nextInt() ; // Sets up scanner to detect variable input by user and place into a int value
  
  // Setting up variables for calculation
  double totalCost; // Declares double variable for totalCost
  double costperPerson; // Declares dobule variable for costperPerson
  int dollars; // Declares int variable for dollars
  int dimes; // Declares int variable for dimes
  int pennies; // Declares int variale for pennies
  totalCost = checkCost * (1+tipPercent); // Sets up equation for values input by user to be calculated to equate and find total cost of the check and tip
  costperPerson = totalCost / numPeople; // Sets up equation to divide the value found previously by the number of people in the party
  dollars = (int) costperPerson; // Converts value equated in previous line to dollar amount 
  dimes = (int) (costperPerson * 10) % 10; // Converts value equated in previous line to dime amount
  pennies = (int) (costperPerson * 100) % 10; // Converts value equated in previous line to penny amount
  System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); // Prints out final calculations to the terminal to show amount owed by each person
  
  
  } //end of main method
} // end of class

 

