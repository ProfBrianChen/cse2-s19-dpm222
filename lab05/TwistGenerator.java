// CSE 02 Lab 02 Dillon Mcandrews 3/4/2019
// Purpose of this program is to be able to detect whether a user has correctly input a positive integer when prompted by using a simple twist 

import java.util.Scanner; 


public class TwistGenerator{
  public static void main(String args[]){
  //Beginning of main method
  
Scanner myScanner = new Scanner ( System.in ); //declaring Scanner
int userInt; // declares int variable for length input
String junkWord;
    
System.out.print ("Input desired length: ");
    
    
//Setting up while loop to restart prompt if wrong input is entered
while (!myScanner.hasNextInt()){ 
junkWord = myScanner.next(); // deletes incorrect input
System.out.print ("Input different desired length: ");
}
    
userInt = myScanner.nextInt() ; // Places correct input into int variable
    
    
    
for (int i = 0; i < userInt; i++){ // beginning of for statement to loop first line of twist with given input
 if (i % 3 == 0){
 System.out.print("\\");
 }
else if (i % 3 == 1){
  System.out.print(" ");
}
else if (i % 3 == 2){
  System.out.print("/");
}
                                
} // end of for statement
    
System.out.println(); //gives space between lines

for (int i = 0; i < userInt; i++){ // beginning of for statement to loop seond line of twist with given input
 if (i % 3 == 0){
 System.out.print(" ");
 }
else if (i % 3 == 1){
  System.out.print("X");
}
else if (i % 3 == 2){
  System.out.print(" ");
}
                                
} // end of for statement
    
System.out.println(); // gives space between lines
    
for (int i = 0; i < userInt; i++){ // beginning of for statement to loop third line of twist with given input
 if (i % 3 == 0){
 System.out.print("/");
 }
else if (i % 3 == 1){
  System.out.print(" ");
}
else if (i % 3 == 2){
  System.out.print("\\");
}
                                
} // end of for statement
    
System.out.println(); // gives space between lines
 
    
    
    
} // end of main method
} // end of public class
