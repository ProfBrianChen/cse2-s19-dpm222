/// CSE 02 HW 02 Dillon McAndrews 2/5/2019
// Program calculates and prints arithmetic sequences pertaining to price of items, salestax, and total operations
//

public class Arithmetic {
   public static void main(String[] args) {
   
   // Setting up values to be used for calculations
   int numPants=3; // gives amount of pants purchased
   double pantsPrice=34.98; // gives price for a pair of pants before sales tax
   int numShirts=2; // gives amount of shirts purchased
   double shirtsPrice=24.99; // gives price for a shirt before sales tax
   int numBelts=1; //gives amount of belts purchased
   double beltsPrice=33.99; //gives price for a belt before sales tax
   double paSalesTax=0.06; //gives number for PA sales tax
     
   // Declaring variables for values (total cost, sales tax, etc.)
   double totalcostofPants = numPants * pantsPrice; // calculates and places total cost of pants into a single variable
   double totalcostofShirts = numShirts * shirtsPrice; // calculates and places total cost of shirts into a single variable
   double totalcostofBelts = numBelts * beltsPrice; // calculates and places total cost of belts into a single variable
   double salestaxchargedonPants = paSalesTax * totalcostofPants; // calculates and places sales tax on all pants purchased into a single variable
   double salestaxchargedonShirts = paSalesTax * totalcostofShirts; // calculates and places sales tax on all shirts purchased into a single variable
   double salestaxchargedonBelts = paSalesTax * totalcostofBelts; // calculates and places sales tax on all belts purchased into a single variable
   double totalcostofPurchase = totalcostofPants + totalcostofShirts + totalcostofBelts; // placeholder for total cost of all items before sales tax 
   double totalSalesTax = salestaxchargedonPants + salestaxchargedonShirts + salestaxchargedonBelts; // placeholder for total sales cost of all items 
   double totalPaid = totalcostofPurchase + totalSalesTax; // placeholder for total amount paid including price of items and sales tax
   
   // Simplifying answers
   double salestaxonPantstimes100 = salestaxchargedonPants * 100; // multiplies sales tax on pants by 100
   double salestaxonShirtstimes100 = salestaxchargedonShirts * 100; // multiplies sales tax on shirts by 100
   double salestaxonBeltstimes100 = salestaxchargedonBelts * 100; // multiplies sales tax on belts by 100
   double totalSalesTaxtimes100 = totalSalesTax * 100; // multiplies total sales tax by 100
   double totalPaidtimes100 = totalPaid * 100; // multiplies total amount paid by 100
     
   int intsalestaxchargedonPants = (int) salestaxonPantstimes100; // converts double value to int
   int intsalestaxchargedonShirts = (int) salestaxonShirtstimes100; // converts double value to int
   int intsalestaxchargedonBelts = (int) salestaxonBeltstimes100; // converts double value to int
   int inttotalsalestax = (int) totalSalesTaxtimes100; // converts double value to int
   int inttotalpaid = (int) totalPaidtimes100; // converts double value to int
     
   int simplesalestaxchargedonPants = intsalestaxchargedonPants / 100; // divides int value by 100
   int simplesalestaxchargedonShirts = intsalestaxchargedonShirts / 100; // divides int value by 100
   int simplesalestaxchargedonBelts = intsalestaxchargedonBelts / 100; // divides int value by 100
   int simpletotalsalestax = inttotalsalestax / 100; // divides int value by 100
   int simpletotalpaid = inttotalpaid / 100;  // divides int value by 100
      
   // Printing out data to terminal
   System.out.println ("The cost of each pair of pants was $" + (pantsPrice)); // Displays cost of each pair of pants
   System.out.println ("The cost of each shirt was $" + (shirtsPrice)); // Displays cost of each shirt
   System.out.println ("The cost of each belt was $" + (beltsPrice)); // Displays cost of each belt
   System.out.println ("The sales tax charged on 3 pairs of pants was $" + (simplesalestaxchargedonPants)); // Displays sales tax charged on all pairs of pants purchased
   System.out.println ("The sales tax charged on 2 shirts was $" + (simplesalestaxchargedonShirts)); // Displays sales tax charged on all shirts purchased
   System.out.println ("The sales tax charged on 1 belt was $" + (simplesalestaxchargedonBelts)); // Displays sales tax charged on all belts purchased
   System.out.println ("The total cost of buying 3 pairs of pants was $" + (totalcostofPants)); // Displays total cost of buying pants before sales tax
   System.out.println ("The total cost of buying 2 shirts was $" + (totalcostofShirts)); // Displays total cost of buying shirts before sales tax
   System.out.println ("The total cost of buying 1 belt was $" + (totalcostofBelts)); // Displays total cost of buying belts before sales tax
   System.out.println ("The total sales tax charged was $" + (simpletotalsalestax)); // Displays total sales tax charged across all items
   System.out.println ("The total amount paid was $" + (simpletotalpaid)); // Displays total amount paid including price of items and sales tax
     
   }
  
}